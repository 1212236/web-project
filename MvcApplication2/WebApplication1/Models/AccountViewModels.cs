﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebApplication1.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Tên tài khoản")]
        public string UserName { get; set; }
    }

    public class ManageUserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu hiện tại")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu mới")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Xác nhận mật khẩu mới")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Tên tài khoản")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [Display(Name = "Duy trì đăng nhập?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "Tên tài khoản")]
        public string UserName { get; set; }
        [Required]
        [Display(Name = "Tên")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Họ")]
        public string LastName { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại mật khẩu")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Email không hợp lệ")]
        public string Email { get; set; }
    }

    public class EditProfileViewModel
    {
        [Required]
        [Display(Name = "Tên")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Họ")]
        public string LastName { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "Tên tài khoản không được bỏ trống")]
        [Display(Name = "Tên tài khoản")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Email không được bỏ trống")]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Email không hợp lệ")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Mật khẩu mới không được bỏ trống")]
        [StringLength(100, ErrorMessage = "{0} phải có ít nhất {2} ký tự", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu mới")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại mật khẩu")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Nhập lại mật khẩu không chính xác")]
        public string ConfirmPassword { get; set; }
    }
    public class AccountListManageViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsDisabled { get; set; }
    }
    public class AccountManagerEditViewModel
    {
        [Required(ErrorMessage = "Tên không được để trống")]
        [Display(Name = "Tên")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Họ không được để trống")]
        [Display(Name = "Họ")]
        public string LastName { get; set; }
    }


    public class AccountManagerRoleSetViewModel
    {
        [Display(Name = "Quyền hiện tại")]
        public string RoleIn { get; set; }
        [Display(Name = "Quyền được cấp")]
        public IEnumerable<SelectListItem> Roles { get; set; }
        public string CurrentRole { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebApplication1.DAL;

namespace WebApplication1.Models
{
    public class OrderViewModel
    {
        [Required(ErrorMessage = "Không được để trống địa chỉ")]
        [Display(Name = "Địa chỉ")]

        public string Address { get; set; }

        [Required(ErrorMessage = "Không được để trống số điện thoại")]
        [RegularExpression(@"(09[0-9]{8})|(01[0-9]{9})", ErrorMessage = "Điện thoại liên lạc chỉ có thể bắt đầu bằng 09 và có tất cả 10 số; hoặc bắt đầu bằng 01 và có tất cả 11 số")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Số điện thoại")]
        public string PhoneNumber { get; set; }

    }

    public class ProductCartViewModel
    {
        public WebApplication1.DAL.Product Product { get; set; }
        [RegularExpression(@"^[0-9]+$")]
        public int Quantity { get; set; }
    }

    public class OrderHistoryItemViewModel
    {
        [DataType(DataType.DateTime)]
        [Display(Name = "Thời điểm đặt hàng")]
        public DateTime? DateOrder { get; set; }
        [Display(Name = "Tổng tiền")]
        public float? Total { get; set; }
        [Display(Name = "Trạng thái")]
        public string Status { get; set; }
    }

    public class TopTenSaleProductStatisticViewModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public float Total { get; set; }
    }
}
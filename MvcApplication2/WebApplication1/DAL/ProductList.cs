﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.DAL;

namespace WebApplication1.Models
{
    public class ProductList
    {
        public Product myproduct { get; set; }
        public IEnumerable<Product> listproduct { get; set; }
        public IEnumerable<Comment> comment { get; set; }
        public Comment mycomment { get; set; }
    }
}
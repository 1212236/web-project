//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication1.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Category
    {
        public Category()
        {
            this.Category_Type = new HashSet<Category_Type>();
        }
    
        public int id { get; set; }
        public string name { get; set; }
    
        public virtual ICollection<Category_Type> Category_Type { get; set; }
    }
}

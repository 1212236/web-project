﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.DAL;

namespace WebApplication1.BLL
{
    public class TypeManager
    {
        ShoeStoreDBEntities ssde = new ShoeStoreDBEntities();
        public void TypeRemove(int id)
        {
            WebApplication1.DAL.Type type = ssde.Types.Single(t => t.id == id);
            List<WebApplication1.DAL.Type> lsttype = ssde.Types.ToList();
            ssde.Types.Remove(type);
            ssde.SaveChanges();
        }

        public void TypeInsert(string typename)
        {
            WebApplication1.DAL.Type type = new DAL.Type() { name = typename };
            ssde.Types.Add(type);
            ssde.SaveChanges();
        }

        public WebApplication1.DAL.Type TypeUpdate(int typeid, string typename)
        {
            WebApplication1.DAL.Type type = ssde.Types.Single(t => t.id == typeid);
            type.name = typename;
            ssde.SaveChanges();
            return type;
        }
    }
}
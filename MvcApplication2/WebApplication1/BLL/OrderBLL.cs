﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.BOL;
using WebApplication1.Models;
using WebApplication1.DAL;

namespace WebApplication1.BLL
{
    public class OrderManager
    {
        ShoeStoreDBEntities ssde = new ShoeStoreDBEntities();
        ProductManager productManager = new ProductManager();
        public Order CreateOrder(string UserId, float Total, string Adddress, string PhoneNumber)
        {
            Order order = new Order
            {
                userid = UserId,
                total = Total,
                address = Adddress,
                phonenumber = PhoneNumber,
                datecreate = DateTime.Now,
                orderstatusid = 2
            };
            Order temp = ssde.Orders.Add(order);
            ssde.SaveChanges();
            return temp;
        }

        public void CreateOrderDetail(int OrderId, int ProductId, int Quantity, float Total)
        {
            OrderDetail odd = new OrderDetail
            {
                orderid = OrderId,
                productid = ProductId,
                quantity = Quantity,
                total = Total,
                sizeid = 1
            };
            ssde.OrderDetails.Add(odd);
            ssde.SaveChanges();
        }

        public List<Order> GetListOrderByUserId(string UserId)
        {
            List<Order> list = ssde.Orders.Where(x => x.userid == UserId).OrderBy(x => x.datecreate).ToList();
            return list;
        }

        public string GetOrderStatusNameById(int? id)
        {
            return ssde.OrderStatus.Single(x => x.id == id).name;
        }

        public List<TopTenProductSale> GetTopTenProductSale()
        {
            // ssde.OrderDetails.GroupBy(x=>x.productid)
            //   .Where()
            var list = (from x in ssde.OrderDetails
                        group x by new { x.productid } into gr
                        select new
                        {
                            gr.Key.productid,
                            quantity = gr.Sum(s => s.quantity),
                            total = gr.Sum(s => s.total)
                        }).OrderByDescending(x => x.quantity).Take(10);
            List<TopTenProductSale> listproduct = new List<TopTenProductSale>();
            foreach (var item in list)
            {
                TopTenProductSale temp = new TopTenProductSale
                {
                    ProductId = item.productid ?? default(int),
                    Quantity = item.quantity ?? default(int),
                    Total = item.total ?? default(int),
                    ProductName = productManager.GetProduct(item.productid ?? default(int)).name
                };
                listproduct.Add(temp);
            }
            return listproduct;
        }
    }
}
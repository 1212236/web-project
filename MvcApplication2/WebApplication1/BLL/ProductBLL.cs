﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;
using WebApplication1.DAL;

namespace WebApplication1.BLL
{
    public class ProductManager
    {
        ShoeStoreDBEntities ssde = new ShoeStoreDBEntities();
        public Product GetProduct(int id)
        {
            return ssde.Products.Find(id);
        }
        //public Product TotalPrice(List<Product> listproduct)
        //{ 
        //    float sum = 0;
        //    foreach(Product product in listproduct)
        //    {
        //        sum += 
        //    }
        //}

        public void ProductDelete(int id)
        {
            WebApplication1.DAL.Product product = ssde.Products.Single(t => t.id == id);
            ssde.Products.Remove(product);

            ssde.SaveChanges();
        }
        public List<Product> FilterbyGender(string gender, List<Product> lstproduct)
        {
            if (gender != "" && gender != null)
            {
                return (from p in lstproduct where p.id_gender.Equals(int.Parse(gender)) select p).ToList();
            }
            return lstproduct;
        }

        public List<Product> FilterbyType(string type, List<Product> lstproduct)
        {

            if (type != "" && type != null)
            {
                List<Product> lstproduct2 = lstproduct.Where(p => p.Type_Product.Where(t => t.id_type == int.Parse(type)).ToList().Count != 0).ToList();

                return lstproduct2;
            }
            return lstproduct;
        }

        public List<Product> Sort(string sortorder, List<Product> lstproduct)
        {
            switch (sortorder)
            {
                case "popular": lstproduct = SortbyNumSold(lstproduct); break;
                case "new": lstproduct = SortbyNewDate(lstproduct); break;
                case "low": lstproduct = SortbyLowPrice(lstproduct); break;
                case "high": lstproduct = SortbyHighPrice(lstproduct); break;
                default: lstproduct = SortbyNumSold(lstproduct); break;
            }
            return lstproduct;
        }

        private List<Product> SortbyHighPrice(List<Product> lstproduct)
        {
            return lstproduct.OrderByDescending(p => p.price).ToList();
        }

        private List<Product> SortbyLowPrice(List<Product> lstproduct)
        {
            return lstproduct.OrderBy(p => p.price).ToList();
        }

        private List<Product> SortbyNewDate(List<Product> lstproduct)
        {
            return lstproduct.OrderBy(p => p.date_create).ToList();
        }

        private List<Product> SortbyNumSold(List<Product> lstproduct)
        {
            return lstproduct.OrderBy(p => p.num_sold).ToList();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using PagedList;
using WebApplication1.DAL;
namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        ShoeStoreDBEntities ssde = new ShoeStoreDBEntities();
        public ActionResult Contact()
        { return View(); }

        public ActionResult Index(int page = 1)
        {
            int numpage = page;
            int pagesize = 3;
            ViewData["numpage"] = numpage;
            List<Product> pr = ssde.Products.OrderByDescending(x => x.date_create).Take(5).ToList();

            return Request.IsAjaxRequest()
                 ? (ActionResult)PartialView("_viewIndex", pr.ToPagedList(numpage, pagesize))
                : View(pr.ToPagedList(numpage, pagesize));
        }

        public PartialViewResult GetMenu()
        {
            ViewData["typelist"] = ssde.Types.ToList();

            return PartialView();
        }

        public ActionResult About()
        { return View(); }

        public ActionResult Home()
        { return View(); }

        public ActionResult details(int id = 0)
        {
            Product product = ssde.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        public ActionResult Login()
        { return View(); }

        public ActionResult Product()
        { return View(); }

        public ActionResult Register()
        { return View(); }

        public ActionResult tempSearch()
        {
            return PartialView();
        }
        public ActionResult Search(FormCollection fc, int? page, string search)
        {

            string name = fc["txtName"];
            string a = "";
            if (search == null)
            {
                a = name;
            }
            else
            {
                a = search;
            }
            if (a != null && a != "")
            {

                var pro = (from p in ssde.Products where (p.name.ToUpper().Contains(a.ToUpper())) orderby p.id descending select p).ToList();

                int pageSize = 3;
                int pageNum = (page ?? 1);

                ViewBag.search = a;
                return Request.IsAjaxRequest()
                 ? (ActionResult)PartialView("_viewSearch", pro.ToPagedList(pageNum, pageSize))
                : View(pro.ToPagedList(pageNum, pageSize));
            }


            return View();
        }



        public ActionResult searchNangCao(int? page, string name, string price, string gender)
        {
            int _gender = Convert.ToInt32(gender);
            int _price = 100000;
            if (!String.IsNullOrEmpty(price) && Convert.ToInt32(price) != null)
            {
                _price = Convert.ToInt32(price);
            }

            string _name = name;
            int pageSize = 3;
            int numpage = (page ?? 1);
            var product = ssde.Products.AsQueryable();
            int temp = 0;
            if (_gender == 0)
            {
                if (!String.IsNullOrEmpty(name) || !String.IsNullOrEmpty(price))
                {
                    if (String.IsNullOrEmpty(name))
                    {
                        product = ssde.Products.Where(p => p.price <= _price).OrderBy(p => p.id);
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(price))
                        {
                            product = ssde.Products.Where(p => p.name.ToLower().Contains(name.ToLower())).OrderBy(p => p.id);
                        }
                        else
                        {
                            product = ssde.Products.Where(p => p.name.ToLower().Contains(name.ToLower()) && p.price < _price).OrderBy(x => x.id);
                        }
                    }

                    temp = 1;
                }
            }
            else
            {
                if (String.IsNullOrEmpty(name))
                {
                    product = ssde.Products.Where(p => p.price <= _price && p.id_gender == _gender).OrderBy(p => p.id);
                }
                else
                {
                    if (String.IsNullOrEmpty(price))
                    {
                        product = ssde.Products.Where(p => p.name.ToLower().Contains(name.ToLower()) && p.id_gender == _gender).OrderBy(p => p.id);
                    }
                    else
                    {
                        product = ssde.Products.Where(p => p.name.ToLower().Contains(name.ToLower()) && p.price < _price && p.id_gender == _gender).OrderBy(x => x.id);
                    }
                }

                temp = 1;
            }

            ViewBag.temp = temp;
            if (temp == 0)
            {
                product = product.OrderBy(x => x.id);
            }
            return Request.IsAjaxRequest()
                 ? (ActionResult)PartialView("_viewSearchNangCao", product.ToPagedList(numpage, pageSize))
                : View(product.ToPagedList(numpage, pageSize));
        }


    }
}
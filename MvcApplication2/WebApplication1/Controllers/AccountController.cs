﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using WebApplication1.Models;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using PagedList;
using PagedList.Mvc;
using CaptchaMvc.HtmlHelpers;
using WebApplication1.Custom;

namespace testemail.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        public AccountController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(model.UserName, model.Password);
                if (user != null && user.IsConfirmed == true && user.IsDisable == false)
                {
                    await SignInAsync(user, model.RememberMe);
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }
        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        // GET: /Account/ConfirmEmail 
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string Token, string Email)
        {
            ApplicationUser user = this.UserManager.FindById(Token);
            if (user != null)
            {
                if (user.Email == Email)
                {
                    user.IsConfirmed = true;
                    await UserManager.UpdateAsync(user);
                    await SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home", new { ConfirmedEmail = user.Email });
                }
                else
                {
                    return RedirectToAction("Confirm", "Account", new { Email = user.Email });
                }
            }
            else
            {
                return RedirectToAction("Confirm", "Account", new { Email = "" });
            }

        }

        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (this.IsCaptchaValid("Captcha is not valid"))
            {
                if (ModelState.IsValid)
                {
                    var user = new ApplicationUser() { UserName = model.UserName };
                    user.Email = model.Email;
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.IsConfirmed = false;
                    var result = await UserManager.CreateAsync(user, model.Password);
                    var roleresult = await UserManager.AddToRoleAsync(user.Id, "member");
                    if (result.Succeeded)
                    {
                        // System.Net.Mail.MailMessage m = new System.Net.Mail.MailMessage(
                        //  new System.Net.Mail.MailAddress("sender@gmail.com", "Web Registration"),
                        //  new System.Net.Mail.MailAddress(user.Email));
                        System.Net.Mail.MailMessage m = new System.Net.Mail.MailMessage();
                        m.To.Add(new System.Net.Mail.MailAddress(user.Email));
                        m.Subject = "Email confirmation";
                        m.Body = string.Format("Dear {0}<BR/>Thank you for your registration, please click on the below link to comlete your registration: <a href=\"{1}\" title=\"User Email Confirm\">{1}</a>", user.UserName, Url.Action("ConfirmEmail", "Account", new { Token = user.Id, Email = user.Email }, Request.Url.Scheme));
                        m.IsBodyHtml = true;
                        using (var smtp = new System.Net.Mail.SmtpClient())
                        {
                            await smtp.SendMailAsync(m);
                            return RedirectToAction("Confirm", "Account", new { Email = user.Email });
                        }
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            else
            {
                ViewBag.CaptchaFailMessage = "Captcha không khớp";
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        [AllowAnonymous]
        public ActionResult Confirm(string Email)
        {
            ViewBag.Email = Email; return View();
        }

        //
        // POST: /Account/Disassociate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        {
            ManageMessageId? message = null;
            IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasPassword)
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            else
            {
                // User does not have a password so remove any validation errors caused by a missing OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var user = await UserManager.FindAsync(loginInfo.Login);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
            }
        }

        //
        // POST: /Account/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        }

        //
        // GET: /Account/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            if (result.Succeeded)
            {
                return RedirectToAction("Manage");
            }
            return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInAsync(user, isPersistent: false);
                        return RedirectToLocal(returnUrl);

                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [CustomAuthorize(Roles = "admin")]
        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        public ActionResult EditProfile(string Message)
        {
            ViewBag.SuccessMessage = Message;
            return View();
        }
        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditProfile(EditProfileViewModel cimodel)
        {
            if (ModelState.IsValid)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                user.FirstName = cimodel.FirstName;
                user.LastName = cimodel.LastName;
                var result = await UserManager.UpdateAsync(user);
                if (!result.Succeeded)
                {
                    AddErrors(result);
                }
                else
                {
                    return RedirectToAction("EditProfile", new { Message = "Cập nhật thông tin thành công" });
                }
            }
            return View();
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        [CustomAuthorize]
        public ActionResult AccountListManage(int? page)
        {
            return View();
        }
        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        [CustomAuthorize(Roles = "admin")]
        public ActionResult AccountListManagePartial(int? page)
        {
            int pagesize = 2;
            int pagenum = (page ?? 1);
            var context = new ApplicationDbContext();
            List<AccountListManageViewModel> model = new List<AccountListManageViewModel>();
            foreach (ApplicationUser user in context.Users)
            {
                model.Add(new AccountListManageViewModel
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    IsDisabled = user.IsDisable
                }
                    );
            }
            ViewBag.LoginUser = User.Identity.GetUserId();
            ViewBag.page = page;
            return PartialView(model.ToList().ToPagedList(pagenum, pagesize));
        }
        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }
        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = UserManager.FindByName(model.Username);
                if (user != null)
                {
                    var result = user.Email == model.Email;

                    if (result)
                    {
                        // System.Net.Mail.MailMessage m = new System.Net.Mail.MailMessage(
                        //  new System.Net.Mail.MailAddress("sender@gmail.com", "Web Registration"),
                        //  new System.Net.Mail.MailAddress(user.Email));
                        System.Net.Mail.MailMessage m = new System.Net.Mail.MailMessage();
                        m.To.Add(new System.Net.Mail.MailAddress(user.Email));
                        m.Subject = "Email confirmation";
                        m.Body = string.Format("Chào {0}<BR/>Bạn hãy click vào link sau để làm mới mật khẩu: <a href=\"{1}\" title=\"User Email Renew\">{1}</a>", user.UserName, Url.Action("RenewPassword", "Account", new { Token = user.Id, Password = model.Password }, Request.Url.Scheme));
                        m.IsBodyHtml = true;
                        using (var smtp = new System.Net.Mail.SmtpClient())
                        {
                            await smtp.SendMailAsync(m);
                            return RedirectToAction("RenewPasswordRequestSend", "Account", new { Email = user.Email });
                        }
                    }
                    else
                    {
                        //AddErrors(result);
                        ViewBag.ErrMessage = "Email không khớp với đăng ký trước đây";
                    }
                }
                else
                {
                    ViewBag.ErrMessage = "Tài khoản không tồn tại";
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }
        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        [AllowAnonymous]
        public ActionResult RenewPasswordRequestSend(string Token, string Password)
        {
            return View();
        }
        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        [AllowAnonymous]
        public async Task<ActionResult> RenewPassword(string Token, string Password)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
            //UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);
            String userId = User.Identity.GetUserId();//"<YourLogicAssignsRequestedUserId>";
            String hashedNewPassword = UserManager.PasswordHasher.HashPassword(Password);
            ApplicationUser cUser = await store.FindByIdAsync(Token);
            await store.SetPasswordHashAsync(cUser, hashedNewPassword);
            await store.UpdateAsync(cUser);
            return View();
        }


        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        //[AllowAnonymous]
        //public async Task<ActionResult> RenewPassword(string Token, string Password)
        //{
        //    ApplicationDbContext context = new ApplicationDbContext();
        //    UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
        //    //UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);
        //    String userId = User.Identity.GetUserId();//"<YourLogicAssignsRequestedUserId>";
        //    String hashedNewPassword = UserManager.PasswordHasher.HashPassword(Password);
        //    ApplicationUser cUser = await store.FindByIdAsync(Token);
        //    await store.SetPasswordHashAsync(cUser, hashedNewPassword);
        //    await store.UpdateAsync(cUser);
        //    return View();
        //}
        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        [CustomAuthorize(Roles = "admin")]
        public ActionResult AccountManagerEdit(string Token, string Message)
        {
            var user = UserManager.FindById(Token);
            AccountManagerEditViewModel model = new AccountManagerEditViewModel { FirstName = user.FirstName, LastName = user.LastName };
            ViewBag.UserName = user.UserName;
            ViewBag.Token = Token;
            ViewBag.SMessage = Message;
            return View(model);
        }
        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "admin")]
        public async Task<ActionResult> AccountManagerEdit(AccountManagerEditViewModel model, string Token)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(Token);
                if (user != null)
                {
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    var result = await UserManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("AccountManagerEdit", new { Token = Token, Message = "Cập nhật thành công" });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }
        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        [CustomAuthorize(Roles = "admin")]
        public async Task<ActionResult> AccountManagerDisable(string Token, int? page)
        {
            if (User.Identity.GetUserId() != Token)
            {
                var user = await UserManager.FindByIdAsync(Token);
                if (user != null)
                {
                    user.IsDisable = user.IsDisable == false ? true : false;
                    var result = await UserManager.UpdateAsync(user);
                    if (!result.Succeeded)
                    {
                        AddErrors(result);
                    }
                }
            }
            return RedirectToAction("AccountListManagePartial", new { page = page });
        }
        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        [CustomAuthorize(Roles = "admin")]
        public ActionResult AccountManagerRoleSet(string Token, string Message)
        {
            var user = UserManager.FindById(Token);
            ApplicationDbContext context = new ApplicationDbContext();
            RoleManager<IdentityRole> rolemanager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            if (context.Roles.Count() != 0)
            {
                AccountManagerRoleSetViewModel model = new AccountManagerRoleSetViewModel();

                model.RoleIn = user.Roles.First().RoleId;
                model.CurrentRole = rolemanager.FindById(model.RoleIn).Name;
                model.Roles = context.Roles.Select(x =>
                    new SelectListItem
                    {
                        Value = x.Id,
                        Text = x.Name
                    }
                );
                ViewBag.UserName = user.UserName;
                ViewBag.Token = Token;
                ViewBag.SMessage = Message;
                return View(model);
            }
            return RedirectToAction("AccountListManage");
        }
        //Có sửa --------------------------------------------------------------------------------------------------------------------------------------->>>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "admin")]
        public async Task<ActionResult> AccountManagerRoleSet(AccountManagerRoleSetViewModel model, string Token)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(Token);
                if (user != null)
                {
                    RoleManager<IdentityRole> rolemanager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

                    string role = model.RoleIn;
                    string rolename = rolemanager.FindById(role).Name;
                    RemoveAllRoleFromUser(Token);
                    var result = await UserManager.AddToRoleAsync(Token, rolename);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("AccountManagerRoleSet", new { Token = Token, Message = "Cấp quyền thành công" });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        bool RemoveAllRoleFromUser(string Token)
        {
            var user = UserManager.FindById(Token);
            if (user != null)
            {
                foreach (IdentityUserRole role in user.Roles.ToList())
                {
                    IdentityUserRole temp = role;
                    user.Roles.Remove(temp);
                }
                return true;
            }
            return false;
        }
        [AllowAnonymous]
        public ActionResult NotAdmin()
        {
            return View();
        }
        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}
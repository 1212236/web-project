﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.BLL;
using WebApplication1.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PagedList;
using PagedList.Mvc;
using WebApplication1.Custom;
using WebApplication1.DAL;

namespace WebApplication1.Controllers
{
    public class OrderController : Controller
    {
        ProductManager productManager = new ProductManager();
        OrderManager orderManager = new OrderManager();
        //
        // GET: /Order/
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult AddToCart(int ProductId)
        {
            var data = Session["cartproduct"];
            List<ProductCartViewModel> list;
            var product = productManager.GetProduct(ProductId);
            if (data != null)
            {
                list = data as List<ProductCartViewModel>;
                var result = list.Find(x => x.Product.id == ProductId);
                if (result != null)
                {
                    result.Quantity++;
                }
                else
                {
                    ProductCartViewModel p = new ProductCartViewModel { Product = product, Quantity = 1 };
                    list.Add(p);
                }

            }
            else
            {
                list = new List<ProductCartViewModel>();
                ProductCartViewModel p = new ProductCartViewModel { Product = product, Quantity = 1 };
                list.Add(p);
            }
            Session["cartproduct"] = list;
            var response = new { Code = "0" };
            return Json(response);
        }

        public ActionResult CartChangeQuantity(int ProductId, int Quantity)
        {
            if (ModelState.IsValid)
            {
                var data = Session["cartproduct"];
                List<ProductCartViewModel> list;

                if (data != null)
                {
                    list = data as List<ProductCartViewModel>;
                    var result = list.Find(x => x.Product.id == ProductId);
                    if (result != null)
                    {
                        result.Quantity = Quantity;
                    }

                }
                else
                {
                    list = new List<ProductCartViewModel>();
                }
                Session["cartproduct"] = list;

            }
            return RedirectToAction("CartPartial");
        }
        public ActionResult Cart()
        {

            return View();
        }

        public ActionResult CartPartial(string Message)
        {
            List<ProductCartViewModel> list;
            var data = Session["cartproduct"];

            if (data != null)
            {
                list = data as List<ProductCartViewModel>;

            }
            else
            {
                list = new List<ProductCartViewModel>();
            }
            float total = TotalPrice(list);
            ViewBag.TotalPrice = total;
            return PartialView(list);
        }
        public ActionResult CartProductDelete(int Token)
        {
            productManager.GetProduct(Token);
            List<ProductCartViewModel> list;
            var data = Session["cartproduct"];
            if (data != null)
            {
                list = data as List<ProductCartViewModel>;
                ProductCartViewModel temp = list.Find(x => x.Product.id == Token);
                list.Remove(temp);
            }
            else
            {
                list = new List<ProductCartViewModel>();
            }
            Session["cartproduct"] = list;
            return RedirectToAction("CartPartial");
        }

        private float TotalPrice(List<ProductCartViewModel> list)
        {
            float s = 0;
            foreach (ProductCartViewModel p in list)
            {
                s += (p.Product.price * p.Quantity);
            }
            return s;
        }
        [Authorize]
        public ActionResult Order()
        {
            ViewData["cart"] = Session["cartproduct"];
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Order(OrderViewModel model)
        {
            if (ModelState.IsValid)
            {
                string userid = User.Identity.GetUserId();
                float totalcart = TotalPrice(GetCart());
                Order order = orderManager.CreateOrder(userid, totalcart, model.Address, model.PhoneNumber);
                List<OrderDetail> orderdetail = new List<OrderDetail>();
                foreach (ProductCartViewModel p in GetCart())
                {
                    orderManager.CreateOrderDetail(order.id, p.Product.id, p.Quantity, p.Product.price * p.Quantity);
                }
                Session["cartproduct"] = null;
                return RedirectToAction("Cart");
            }
            return View();
        }

        [Authorize]
        public ActionResult OrderHistory()
        {
            return View();
        }

        [Authorize]
        public ActionResult OrderHistoryPartial(int? page)
        {
            int pagesize = 5;
            int pagenum = (page ?? 1);
            string id = User.Identity.GetUserId();
            List<Order> list = orderManager.GetListOrderByUserId(id);
            List<OrderHistoryItemViewModel> listview = new List<OrderHistoryItemViewModel>();

            foreach (Order o in list)
            {
                OrderHistoryItemViewModel oh = new OrderHistoryItemViewModel
                {
                    DateOrder = o.datecreate,
                    Status = orderManager.GetOrderStatusNameById(o.orderstatusid),
                    Total = o.total
                };
                listview.Add(oh);
            }
            return PartialView(listview.ToPagedList(pagenum, pagesize));
        }

        private List<ProductCartViewModel> GetCart()
        {
            List<ProductCartViewModel> list;
            var data = Session["cartproduct"];

            if (data != null)
            {
                list = data as List<ProductCartViewModel>;

            }
            else
            {
                list = new List<ProductCartViewModel>();
            }
            return list;
        }
        [CustomAuthorize(Roles = "admin")]
        public ActionResult TopTenSaleProductStatistic()
        {
            List<TopTenSaleProductStatisticViewModel> list = ConvertToTopTenListModel(orderManager.GetTopTenProductSale());
            return View(list);
        }
        private List<TopTenSaleProductStatisticViewModel> ConvertToTopTenListModel(List<BOL.TopTenProductSale> list)
        {
            List<TopTenSaleProductStatisticViewModel> listresult = new List<TopTenSaleProductStatisticViewModel>();
            foreach (var i in list)
            {
                TopTenSaleProductStatisticViewModel temp = new TopTenSaleProductStatisticViewModel
                {
                    ProductId = i.ProductId,
                    ProductName = i.ProductName,
                    Quantity = i.Quantity,
                    Total = i.Total
                };
                listresult.Add(temp);
            }
            return listresult;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.DAL;
using PagedList;
using WebApplication1.BLL;

namespace WebApplication1.Controllers
{
    public class ProductController : Controller
    {
        ShoeStoreDBEntities ssde = new ShoeStoreDBEntities();
        ProductManager productManager = new ProductManager();
        TypeManager typeManager = new TypeManager();
        //
        // GET: /Product/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult View1()
        {
            return View(ssde.Types.ToList());
        }

        public ActionResult ProductList(string sortorder, string gender, string type, int? page)
        {
            int pagesize = 3;
            int pagenum = (page ?? 1);
            ViewData["typelist"] = ssde.Types.ToList();
            List<Product> lstproduct = ssde.Products.ToList();
            lstproduct = productManager.FilterbyGender(gender, lstproduct);
            lstproduct = productManager.FilterbyType(type, lstproduct);
            lstproduct = productManager.Sort(sortorder, lstproduct);
            ViewData["gender"] = gender;
            ViewData["type"] = type;
            ViewData["sortorder"] = sortorder;
            return View(lstproduct.ToPagedList(pagenum, pagesize));
        }

        
        
        public ActionResult ProductsList(int? page)
        {

            int pagesize = 2;
            int pagenum = (page ?? 1);

            return View((from p in ssde.Products orderby p.id select p).ToPagedList(pagenum, pagesize));
        }


        public ActionResult Addcomment(ProductList comment, int id, int page = 1)
        {

            int numpage = (int)TempData["pagenum1"];
            if (ModelState.IsValid)
            {
                int i = ssde.Comments.Count();
                comment.mycomment.id = i + 1;
                comment.mycomment.id_product = id;
                comment.mycomment.date = DateTime.Now;
                ssde.Comments.Add(comment.mycomment);
                //int parameter = Convert.ToInt32(Session["Id"].ToString());
                ssde.SaveChanges();
                var cm = ssde.Comments.ToList();
                var model = new ProductList();
                model.comment = cm;
                return RedirectToAction("ProductDetails");
            }
            return Json(comment);
        }

        public ActionResult ProductDetails(int? page, int id = 1)
        {
            int temp = 0;
            int pagenum = (page ?? 1);

            var _listproduct = ssde.Products;
            Product _myproduct = ssde.Products.Find(id);
            var comment = ssde.Comments.Where(x => x.id_product == id).OrderBy(x => x.id).Skip(((pagenum - 1) * pagetypesize)).Take(pagetypesize);
            if (_myproduct == null)
            {
                return HttpNotFound();
            }

            WebApplication1.Models.ProductList model = new Models.ProductList();
            model.listproduct = _listproduct.ToList();
            model.myproduct = _myproduct;
            model.comment = comment;
            ViewBag.temp = temp;
            ViewData["pagenum"] = pagenum;
            TempData["pagenum1"] = pagenum;
            ViewBag.id = id;
            ViewBag.Numpage = pagenum;
            ViewBag.Totalpage = (ssde.Comments.Count() / pagetypesize) + 1;
            temp++;
            return Request.IsAjaxRequest()
               ? (ActionResult)PartialView("_viewComment", model)
              : View(model);
        }
        int pagetypesize = 3;
        public ActionResult ProductType(int? page)
        {
            int pagenum = (page ?? 1);
            List<WebApplication1.DAL.Type> lsttype = ssde.Types.ToList();
            ViewData["pagenum"] = pagenum;
            return View(lsttype.ToPagedList(pagenum, pagetypesize));
        }

        public ActionResult ProductTypeDelete(int id, int? page)
        {
            int pagenum = (page ?? 1);

            typeManager.TypeRemove(id);
            
            return RedirectToAction("ProductType");
            //return View("ProductType", lsttype.ToPagedList(pagenum, pagetypesize));
        }

        public ActionResult ProductTypeInsert()
        {
            return View();
        }

        public ActionResult InsertType(FormCollection fc)
        {
            string typename = fc["name"].ToString();
            
            return View("ProductTypeInsert");
        }

        public ActionResult ProductTypeUpdate(int id)
        {
            WebApplication1.DAL.Type type = ssde.Types.Single(t => t.id == id);
            return View(type);
        }

        public ActionResult UpdateType(FormCollection fc)
        {
            int typeid = int.Parse(fc["id"]);
            string typename = fc["name"];
            
            return View("ProductTypeUpdate", typeManager.TypeUpdate(typeid, typename));
        }

        public ActionResult ProductManage(int? page)
        {
            int pagenum = (page ?? 1);
            List<WebApplication1.DAL.Product> lstproduct = ssde.Products.ToList();
            ViewData["pagenum"] = pagenum;
            return View(lstproduct.ToPagedList(pagenum, pagetypesize));
        }

        public ActionResult ProductDelete(int id, int? page)
        {
            int pagenum = (page ?? 1);

            productManager.ProductDelete(id);
            return RedirectToAction("ProductManage");
        }
        protected override void Dispose(bool disposing)
        {
            ssde.Dispose();
            base.Dispose(disposing);
        }
    }
}
